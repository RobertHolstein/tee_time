import os
from dotenv import load_dotenv, find_dotenv
import discord
from discord.ext import commands, tasks
import requests_async as requests
import urllib.parse
from tabulate import tabulate
import asyncio
from datetime import datetime
import pandas as pd
import math
import pytz
import logging
from http import HTTPStatus

load_dotenv()
BOT_TOKEN = os.getenv('BOT_TOKEN')
CHANNEL_ID = os.getenv('CHANNEL_ID')
COURSES = os.getenv('COURSES').split(",")
PROGRAM_ID = os.getenv('PROGRAM_ID')
USER = os.getenv('USER')
PASSWORD = os.getenv('PASSWORD')
INTERVAL_MINUTES = int(os.getenv('INTERVAL_MINUTES'))
TOKEN_UPDATE_INTERVAL_MINUTES = int(os.getenv('TOKEN_UPDATE_INTERVAL_MINUTES'))
LAST_FOUR = os.getenv('LAST_FOUR')
PICK_FIRST_CARD = bool(os.getenv('PICK_FIRST_CARD'))
ALLOWED_USERS = set(map(int, os.getenv('ALLOWED_USERS').split(',')))
ALLOWED_CHANNEL_ID = int(os.getenv('ALLOWED_CHANNEL_ID'))

TOKEN = None
HAS_CARD_ON_FILE = False

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='!', intents=intents)

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

@bot.listen()
async def on_ready():
    logger.info(f"def on_ready()")
    update_token_loop.start()

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    
    if not message.content.startswith("!"):
        return
    
    if message.channel.id != ALLOWED_CHANNEL_ID:
        logger.info(f"User [{message.author.id}] tried using tee-time-bot in the wrong channel [{message.channel.id}]")
        return
    
    channel = await bot.fetch_channel(CHANNEL_ID)
    if message.author.id not in ALLOWED_USERS:
        await channel.send(f":warning: {message.author.mention} you are not allowed to use this command :warning:")
        logger.info(f"User [{message.author.id}] tried using a tee-time-bot protected action. They are not in the allowed users list.")
        return
    
    if TOKEN is None or HAS_CARD_ON_FILE is False:
        await update_info()

    if TOKEN is None:
        logger.error(f"No token available")
        await channel.send(":warning: The bot was unable to sign in with the credentials you provided. Update your credentials. :warning:")
        return
    
    if HAS_CARD_ON_FILE is False:
        logger.warning("No credit card available for this account")
        await channel.send(":warning: There are no credit cards assosiated with your account. Please visit the website and add one. :warning:")
        return
    
    await bot.process_commands(message)

@bot.command(help="Updates user token and credit card info")
async def update(ctx):
    await update_info()

async def update_info():
    logger.info(f"def update")
    global HAS_CARD_ON_FILE
    global TOKEN
    TOKEN = await get_login_token()
    logger.info(f"token created [{TOKEN}]")
    if await get_credit_cards():
        logger.info(f"At least one credit card found")
        HAS_CARD_ON_FILE = True

@bot.command(help="Lists available courses")
async def courses(ctx):
    logger.info(f"bot command [courses] called by [{ctx.author.id}]")
    logger.info(f"def courses")
    # Create a message string that lists the courses with their index number
    courses_list = "\n".join([f"{index}: {course}" for index, course in enumerate(COURSES)])
    message = (
        f":golf: **Available Courses** :golf:\n"
        f"```{courses_list}```"
    )
    # Send the courses list to the user
    await ctx.send(message)

@bot.command(help="Sets the interval for the search loop")
async def interval(ctx, interval_minutes: int):
    logger.info(f"bot command [interval] called by [{ctx.author.id}]")
    logger.info(f"def interval() params: [{interval_minutes}]")
    global INTERVAL_MINUTES
    INTERVAL_MINUTES = interval_minutes
    search_tee_times_loop.change_interval(minutes=interval_minutes)
    channel = await bot.fetch_channel(CHANNEL_ID)
    await channel.send(f":golf::man_golfing: The tee time request interval has been changed to {interval_minutes}. If you had a tee time request going, it will continue with the new interval.")

@bot.command(
    help="Searches for available tee times",
    brief="!search <course_ids_string> <number_of_players> <date_min> <date_max> <time_min> <time_max>"
)
async def search(
    ctx,
    course_ids_string: str = commands.parameter(
        description="A comma-separated list of course IDs to search for tee times"
    ),
    number_of_players: str = commands.parameter(
        description="The number of players to book for"
    ),
    date_min: str = commands.parameter(
        description="The minimum date to search for tee times (format: YYYY-MM-DD)"
    ),
    date_max: str = commands.parameter(
        description="The maximum date to search for tee times (format: YYYY-MM-DD)"
    ),
    time_min: str = commands.parameter(
        description="The minimum time to search for tee times (format: HH:mm)"
    ),
    time_max: str = commands.parameter(
        description="The maximum time to search for tee times (format: HH:mm)"
    )
):
    logger.info(f"bot command [search] called by [{ctx.author.id}]")
    logger.info(f"def search() params:  [{course_ids_string}] [{number_of_players}] [{date_min}] [{date_max}] [{time_min}] [{time_max}]")
    if search_tee_times_loop.is_running():
        stop_search_tee_times_loop()
    should_book = False
    search_tee_times_loop.start(course_ids_string, number_of_players, date_min, date_max, time_min, time_max, should_book)
    await ctx.send(f":golf::man_golfing: Checking for open spots at that date and time.")


@bot.command(
    help="Searches and books an available tee time",
    brief="!searchandbook <course_ids_string> <number_of_players> <date_min> <date_max> <time_min> <time_max>"
)
async def searchandbook(
    ctx,
    course_ids_string: str = commands.parameter(
        description="A comma-separated list of course IDs to search for tee times"
    ),
    number_of_players: str = commands.parameter(
        description="The number of players to book for"
    ),
    date_min: str = commands.parameter(
        description="The minimum date to search for tee times (format: YYYY-MM-DD)"
    ),
    date_max: str = commands.parameter(
        description="The maximum date to search for tee times (format: YYYY-MM-DD)"
    ),
    time_min: str = commands.parameter(
        description="The minimum time to search for tee times (format: HH:mm)"
    ),
    time_max: str = commands.parameter(
        description="The maximum time to search for tee times (format: HH:mm)"
    )
):
    logger.info(f"bot command [searchandbook] called by [{ctx.author.id}]")
    logger.info(f"def searchandbook() params: [{course_ids_string}] [{number_of_players}] [{date_min}] [{date_max}] [{time_min}] [{time_max}]")
    if search_tee_times_loop.is_running():
        stop_search_tee_times_loop()
    should_book = True
    search_tee_times_loop.start(course_ids_string, number_of_players, date_min, date_max, time_min, time_max, should_book)
    await ctx.send(f":golf::man_golfing: Checking for open spots at that date and time. If a tee time is found it will be booked")

@bot.command(help="Cancels a booked tee time")
async def cancel(ctx, booking_id):
    logger.info(f"bot command [cancel] called by [{ctx.author.id}]")
    logger.info(f"def cancel() params: [{booking_id}]")
    cancelation = await cancel_booking(booking_id)
    message = (
        f":warning: **TEE TIME CANCELED** :warning:\n"
        f"```"
        f"Course:              {cancelation['course']['name']}\n"
        f"Tee Off Time:        {cancelation['tee_off_at_local']}\n"
        f"Number Of Players:   {cancelation['players']}\n"
        f"Booking Status:      {cancelation['booking_status']}\n"
        f"Cancellable Until:   {cancelation['cancellable_until_at_local']}\n"
        f"Cancelled At:        {cancelation['cancelled_at']}\n"
        f"No Show Amount:      {cancelation['no_show_amount']}\n"
        f"Cancellation Policy: {cancelation['cancellation_policy']}\n"
        f"```"
    )
    await ctx.send(message)

@bot.command(help="Stops the tee time search loop")
async def stop(ctx):
    logger.info(f"bot command [stop] called by [{ctx.author.id}]")
    logger.info(f"def stop()")
    if search_tee_times_loop.is_running():
        stop_search_tee_times_loop()
        await ctx.send("The tee time search has been stopped.")

@bot.command(help="Displays the status of the tee time search loop")
async def status(ctx):
    logger.info(f"bot command [status] called by [{ctx.author.id}]")
    logger.info(f"def status()")
    status_message = check_status()
    await ctx.send(status_message)

@bot.command(help="Displays user information")
async def user(ctx):
    logger.info(f"bot command [user] called by [{ctx.author.id}]")
    logger.info(f"def status()")
    info = await get_user_info()
    message = (
        f":information_source: **USER INFO** :information_source:\n"
        f"```"
        f"id:              {info['id']}\n"
        f"email:           {info['email']}\n"
        f"firstName:       {info['firstName']}\n"
        f"lastName:        {info['lastName']}\n"
        f"countryISO:      {info['countryISO']}\n"
        f"country:         {info['country']}\n"
        f"zipCode:         {info['zipCode']}\n"
        f"isEmailEditable: {info['isEmailEditable']}\n"
        f"addressLine1:    {info['addressLine1']}\n"
        f"addressLine2:    {info['addressLine2']}\n"
        f"phoneNumber:     {info['phoneNumber']}\n"
        f"city:            {info['city']}\n"
        f"state:           {info['state']}\n"
        f"```"
    )
    await ctx.send(message)

@tasks.loop(minutes=TOKEN_UPDATE_INTERVAL_MINUTES)
async def update_token_loop():
    logger.info(f"def update_token_loop()")
    await update_info()

@tasks.loop(minutes=INTERVAL_MINUTES)
async def search_tee_times_loop(course_ids_string: str, number_of_players: str, date_min: str, date_max: str, time_min: str, time_max: str, should_book: bool):
    try:
        logger.info(f"def search_tee_times_loop() params: [{course_ids_string}] [{number_of_players}] [{date_min}] [{date_max}] [{time_min}] [{time_max}] [{should_book}]")
        if search_tee_times_loop.current_loop == 0:
            search_tee_times_loop.loop_params = {
                'course_ids_string': course_ids_string,
                'number_of_players': number_of_players,
                'date_min': date_min,
                'date_max': date_max,
                'time_min': time_min,
                'time_max': time_max,
                'should_book': should_book,
                'start_time': datetime.now()
            }

        courses = select_courses(course_ids_string.split(','), COURSES)
        tee_times = await get_all_tee_times_date_time(courses, number_of_players, date_min, date_max, time_min, time_max)
        if tee_times:
            channel = await bot.fetch_channel(CHANNEL_ID)
            if should_book:
                earliest_tee_time = min(tee_times, key=lambda x: x['date_time'])
                rates = await get_teetime_rates(earliest_tee_time['full_course_name'], earliest_tee_time['course'], number_of_players, earliest_tee_time['date_time'])
                regular_rate = next(rate for rate in rates if rate['type'] == 'is_regular_rate')
                
                tee_time = await get_tee_times(
                                regular_rate['num_holes'],
                                regular_rate['cart_type'],
                                regular_rate['major_rate_type'],
                                regular_rate['minor_rate_type'],
                                earliest_tee_time['date_time'],
                                number_of_players,
                                earliest_tee_time['full_course_name'])
                
                the_tee_time = tee_time['tee_times'][0]

                reservation = await prepare_reservation(number_of_players, True, the_tee_time['uuid'], the_tee_time['tee_off_at_local'], the_tee_time['id'])
                user_info =  await get_user_info()
                credit_cards = await get_credit_cards()
                if PICK_FIRST_CARD:
                    selected_credit_card = credit_cards[0]
                else:
                    selected_credit_card = next((card for card in credit_cards if card['last_four'] == LAST_FOUR), None)

                booking = await book_tee_time(reservation['token'], selected_credit_card['id'], user_info['firstName'], user_info['lastName'], user_info['email'])
                message = (
                    f"@here\n"
                    f":golf: **TEE TIME BOOKED** :man_golfing:\n"
                    f"```"
                    f"Course:            {booking['offer']['course_name']}\n"
                    f"Tee Off Time:      {booking['offer']['tee_time']['tee_off_at_local']}\n"
                    f"Number of Players: {booking['offer']['qty']}\n"
                    f"Rate:              {booking['offer']['rate']['symbol']}{booking['offer']['rate']['amount']} {booking['offer']['rate']['currency']}\n"
                    f"Total Due:         {booking['offer']['total_due']['symbol']}{booking['offer']['total_due']['amount']} {booking['offer']['total_due']['currency']}\n"
                    f"Cancel prompt:     !cancel {booking['offer']['tee_time_reservation_id']}\n"
                    f"```"
                )
                message = await channel.send(message)
            else:
                message = await channel.send(f"@here :golf: **TEE TIME(S) FOUND** :man_golfing:")
                thread = await message.create_thread(name="Tee Time")
                for tee_time in tee_times:
                    await thread.send(f"----------------------------------------")
                    await thread.send(f"{tee_time['link']}")
                    await thread.send(f"```Course: {tee_time['course']}\rDate and Time: {tee_time['date_time']}```")
                    await thread.send(f"----------------------------------------")
            stop_search_tee_times_loop()
    except asyncio.CancelledError:
        logger.info("Tee times search stopped")
        search_tee_times_loop.loop_params = None
    except Exception as e:
        # Log or handle the exception as needed.
        logger.error(f"An error occurred: {e}")
        search_tee_times_loop.loop_params = None

def check_status():
    logger.info(f"def check_status()")
    if search_tee_times_loop.is_running():
        params = search_tee_times_loop.loop_params
        utc = pytz.UTC
        start_time = params['start_time'].astimezone(utc).strftime("%Y-%m-%d %H:%M:%S")
        next_run = search_tee_times_loop.next_iteration.astimezone(utc).strftime("%Y-%m-%d %H:%M:%S")
        status_message = (
            "The tee time search is currently running with the following parameters:\n"
            f"```"
            f"Start Time:        {start_time} UTC\n"
            f"Next Run Time:     {next_run} UTC\n"
            f"Sleep Minutes:     {INTERVAL_MINUTES}\n"
            f"Current Loop:      {search_tee_times_loop.current_loop + 1}\n"
            f"Course IDs:        {params['course_ids_string']}\n"
            f"Number of players: {params['number_of_players']}\n"
            f"Date range:        {params['date_min']} - {params['date_max']}\n"
            f"Time range:        {params['time_min']} - {params['time_max']}\n"
            f"Should book:       {'Yes' if params['should_book'] else 'No'}"
            f"```"
        )
    else:
        status_message = "The tee time loop is not running."
    return status_message

def stop_search_tee_times_loop():
    logger.info(f"def stop_search_tee_times_loop()")
    logger.info(f"canceling search_tee_times_loop task loop")
    search_tee_times_loop.cancel()
    search_tee_times_loop.loop_params = None

async def check_request_status(response, error_message):
    if response.status_code != HTTPStatus.OK:
        logger.error(f"{error_message}\nStatus Code: {response.status_code}\nError: {response.text}")
        channel = await bot.fetch_channel(CHANNEL_ID)
        await channel.send(f"```{error_message}\nStatus Code: {response.status_code}\nError: {response.text}```")
        return False
    return True

async def get_login_token():
    logger.info(f"def get_login_token()")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/authentication/signin?programId={PROGRAM_ID}"
    headers = {
        "Content-Type": "application/json"
    }
    payload = {
        "programId": PROGRAM_ID,
        "username": USER,
        "password": PASSWORD
    }
    response = await requests.post(url, headers=headers, json=payload)
    if not await check_request_status(response, "Failed to get a valid token."):
        return None
    response_json = response.json()
    return response_json['token']

async def get_teetimes_for_course(courseName: str, date: str, number_of_players: str):
    logger.info(f"def get_teetimes_for_course() params: [{courseName}] [{date}] [{number_of_players}]")
    # Replace dashes with spaces and encode string
    courseName = urllib.parse.quote_plus(courseName)
    date = urllib.parse.quote_plus(date)

    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/courses/reservations_group?allCartSelected=true&allRatesSelected=true&date={date}&max_hour=21&max_price=500&min_hour=5&min_price=0&slug={courseName}&programId=57&qty={number_of_players}"
    response = await requests.get(url)
    if not await check_request_status(response, "Failed to get tee times for course."):
        return None
    data = response.json()

    # Extract the relevant data from the response
    if data['tee_time_groups']:
        tee_time_groups = []
        for tee_time in data['tee_time_groups']:
            full_course_name = courseName
            course = courseName.replace("-", " ").rsplit(" ", 1)[0]
            date_format = "%Y-%m-%dT%H:%M:%S.%fZ"
            date_time = datetime.strptime(tee_time['tee_off_at_local'], date_format)
            date = f"{tee_time['tee_off_at_local'].split('T')[0]}"
            time = f"{tee_time['tee_off_at_local'].split('T')[1]}"
            starting_rate = f"{tee_time['symbol']}{tee_time['starting_rate']:.2f}"
            max_regular_rate = f"{tee_time['symbol']}{tee_time['max_regular_rate']:.2f}"
            players = ', '.join(map(str, tee_time['players']))

            time_obj = datetime.strptime(tee_time['tee_off_at_local'], "%Y-%m-%dT%H:%M:%S.%fZ")
            link = tee_time_rates_link_generator(courseName, course, tee_time['players'], time_obj)
            
            tee_time_group = {
                "full_course_name": full_course_name,
                "course": course,
                "date_time": date_time,
                "starting_rate": starting_rate,
                "max_regular_rate": max_regular_rate,
                "players": players,
                "link": link
            }
            tee_time_groups.append(tee_time_group)
        return tee_time_groups


async def get_all_tee_times_date_time(courses: list, number_of_players: str, date_min: str, date_max: str, time_min: str, time_max: str):
    logger.info(f"def get_all_tee_times_date_time() params: [{courses}] [{number_of_players}] [{date_min}] [{date_max}] [{time_min}] [{time_max}]")
    start = datetime.strptime(date_min, "%Y-%m-%d")
    end = datetime.strptime(date_max, "%Y-%m-%d")
    dates = pd.date_range(start,end).to_pydatetime().tolist()

    all_tee_times = []
    for course in courses:
        for date in dates:
            tee_time_for_course = await get_teetimes_for_course(course, date.strftime("%Y-%m-%d"), number_of_players)
            if tee_time_for_course:
                all_tee_times.extend(tee_time_for_course)

    tee_times_between_times = []
    time_min_dt = datetime.strptime(time_min, '%H:%M').time()
    time_max_dt = datetime.strptime(time_max, '%H:%M').time()
    for tee_time in all_tee_times:
        if time_min_dt <= tee_time['date_time'].time() <= time_max_dt:
            tee_times_between_times.append(tee_time)

    return tee_times_between_times

def tee_time_rates_link_generator(full_course_name: str, course_name: str, players: list, date_time: datetime):
    logger.info(f"def tee_time_rates_link_generator() params: [{full_course_name}] [{course_name}] [{players}] [{date_time}]")
    date_slot = date_time.strftime("%Y-%m-%d")
    time_slot = date_time.strftime("%I:%M:%S %p")
    if len(players) == 1:
        players_string = "1"
    else:
        players_string = f"{min(players)} - {max(players)}"
    body = f"allCartSelected=true&allRatesSelected=true&courseName={course_name}&date={date_slot}&holesGroupText=18&max_hour=21&max_price=500&min_hour=5&min_price=0&playersGroupText={players_string}&time_slot={time_slot}&transportText=Cart Available"
    body_encoded = urllib.parse.quote_plus(body, safe='=&')
    return f"https://letsgo.golf/recreation-park-golf-course-18/teeTimeRates/at/{full_course_name}?{body_encoded}"

async def get_user_info():
    logger.info(f"def get_user_info()")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/userProfile?programId={PROGRAM_ID}"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=UTF-8",
        "x-api-user-token": TOKEN
    }
    response = await requests.get(url, headers=headers)
    if not await check_request_status(response, "Failed to get user info."):
        return None
    data = response.json()
    return data

async def get_credit_cards():
    logger.info(f"def get_credit_cards()")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/payment/creditcards?programId={PROGRAM_ID}"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=UTF-8",
        "x-api-user-token": TOKEN
    }
    response = await requests.get(url, headers=headers)
    if not await check_request_status(response, "Failed to get credit cards."):
        return None
    data = response.json()
    return data

async def book_tee_time(reservation_id, credit_card_id, first_name, last_name, email):
    logger.info(f"def book_tee_time() params: [{reservation_id}] [{credit_card_id}] [{first_name}] [{last_name}] [{email}]")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/reservation/{reservation_id}?&programId={PROGRAM_ID}"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=UTF-8",
        "x-api-user-token": TOKEN
    }
    payload = {
        "credit_card_id": credit_card_id,
        "userReservationsDetails": {
            "FirstName": first_name,
            "LastName": last_name,
            "Email": email
        }
    }
    response = await requests.post(url, headers=headers, json=payload)
    if not await check_request_status(response, "Failed to book a team time."):
        return None
    data = response.json()
    return data['receipt']

async def cancel_booking(booking_id):
    logger.info(f"def cancel_booking() params: [{booking_id}]")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/reservation/cancel/{booking_id}?programId={PROGRAM_ID}"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=UTF-8",
        "x-api-user-token": TOKEN
    }
    response = await requests.post(url, headers=headers)
    if not await check_request_status(response, "Failed to cancel booking."):
        return None
    data = response.json()
    return data['reservation']

async def prepare_reservation(qty, with_default_credit_card, uuid, tee_time_date, tee_time_id):
    logger.info(f"def prepare_reservation() params: [{qty}] [{with_default_credit_card}] [{uuid}] [{tee_time_date}] [{tee_time_id}]")
    url = f"https://sg-membership20-portalapi-production.azurewebsites.net/api/courses/tee_times/{tee_time_id}/reservations/prepare?programId={PROGRAM_ID}"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=UTF-8",
        "x-api-user-token": TOKEN
    }
    payload = {
        "qty": qty,
        "with_default_credit_card": with_default_credit_card,
        "uuid": uuid,
        "teeTimeDate": tee_time_date
    }
    response = await requests.post(url, headers=headers, json=payload)
    if not await check_request_status(response, "Failed to prepare reservation."):
        return None
    data = response.json()
    return data['prepared_tee_time']

async def get_teetime_rates(full_course_name: str, course_name: str, players: list, date_time: datetime):
    logger.info(f"def get_teetime_rates() params: [{full_course_name}] [{course_name}] [{players}] [{date_time}]")
    date_slot = date_time.strftime("%Y-%m-%d")
    time_slot = date_time.strftime("%I:%M:%S %p")
    params = {
        "courseName": course_name,
        "date": date_slot,
        "is_riding": None,
        "max_price": "500",
        "min_price": "0",
        "slug": full_course_name,
        "time_slot": time_slot,
        "programId": PROGRAM_ID
    }
    url = "https://sg-membership20-portalapi-production.azurewebsites.net/api/courses/tee_time_groups_rate_types"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "x-api-user-token": TOKEN
    }
    response = await requests.get(url, params=params, headers=headers)
    if not await check_request_status(response, "Failed to get tee time rates."):
        return None
    data = response.json()
    return data['rates']

async def get_tee_times(num_holes, cart_type, major_rate_type, minor_rate_type, date_time, qty, full_course_name):
    logger.info(f"def get_teetime_rates() params: [{num_holes}] [{cart_type}] [{major_rate_type}] [{minor_rate_type}] [{date_time}] [{qty}] [{full_course_name}]")
    date_slot = date_time.strftime("%Y-%m-%d")
    time_slot = date_time.strftime("%I:%M:%S %p")
    is_riding = True if cart_type == 'is_riding' else False
    
    params = {
        "date": date_slot,
        "is_riding": is_riding,
        "major_rate_type": major_rate_type,
        "minor_rate_type": minor_rate_type,
        "num_holes": num_holes,
        "qty": qty,
        "slug": full_course_name,
        "time_slot": time_slot,
        "programId": PROGRAM_ID
    }
    url = "https://sg-membership20-portalapi-production.azurewebsites.net/api/courses/tee_time_at"
    headers = {
        "Accept": "application/json, text/plain, */*",
        "x-api-user-token": TOKEN
    }
    response = await requests.get(url, params=params, headers=headers)
    if not await check_request_status(response, "Failed to get tee times."):
        return None
    data = response.json()
    return data

def select_courses(index_strings, all_courses):
    logger.info(f"def select_courses() params: [{index_strings}] [{all_courses}]")
    indices = [int(index_str) for index_str in index_strings]
    selected_courses = [all_courses[index] for index in indices]
    return selected_courses

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(bot.start(BOT_TOKEN))
    loop.run_forever()